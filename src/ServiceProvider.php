<?php
namespace LaravelAttach;

use Illuminate\Support\ServiceProvider as LServiceProvider;

class ServiceProvider extends LServiceProvider {


    public function boot()
    {
        //$this->package('laravel-addons/attach');
        require __DIR__.'/routes.php';
    }

    public function register()
    {

        //$this->app['config']->package('laravel-addons/attach', __DIR__.'/../config');

    }


    public function provides()
    {
        return array();
    }
}